import os
import glob
import pandas as pd

for filepath in glob.glob("./*.txt"):
    basename_without_ext = os.path.splitext(os.path.basename(filepath))[0]
    output_csv = "./output/" + basename_without_ext + ".csv"
    if os.path.isfile(output_csv):
        print(pd.read_csv(output_csv))
        print("\n")
