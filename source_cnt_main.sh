#!/bin/bash -ue

items=(
    "VNMS.txt"
    "ASCX.txt"
    "ACME.txt"
    "INFRA.txt"
    "FGL3F.txt"
)

CDPWD=`pwd`

rm -rf ${CDPWD}/output/*

for FILEA in "${items[@]}" ;
do
    FILEB=`echo ${FILEA} | sed 's/\.[^\.]*$//'`
    OUTPUTCSV="${CDPWD}/output/${FILEB}.csv"

    echo "Type,Repo,AllCounts,ExcludeTestCounts" > ${OUTPUTCSV}

    cat ${CDPWD}/${FILEA} | while read line
    do
        REPO=`echo $line | awk -F'@' '{print $1}'`
        BRANCH=`echo $line | awk -F'@' '{print $2}'`
        PARAM=`echo $line | awk -F'@' '{print $3}'`

        if [ ! -d ${REPO} ]; then
            git clone git@bitbucket.org:fgl-smartbuilding/${REPO}.git
            if [ $? -ne 0 ]; then
                echo ${REPO} >> ${CDPWD}/output/${FILEB}_ERR.csv
            fi
            sleep 1s
        fi

        cd ${REPO}
        echo ${REPO}

        git fetch -pa
        git checkout ${BRANCH}
        sleep 1s
        git pull > /dev/null 2>&1
        cd ..

        #source-code all counts
        ALLCNT=`${CDPWD}/cloc-1.92.pl ${REPO} | awk '(NF==5 && $1=="SUM:") {printf("%s",$5)}'`
        #source-code exclude counts
        EXCLUDESTR="${CDPWD}/cloc-1.92.pl ${REPO} ${PARAM}"
        EXCLUDECNT=`eval ${EXCLUDESTR} | awk '(NF==5 && $1=="SUM:") {printf("%s",$5)}'`

        echo "${FILEB},${REPO},${ALLCNT},${EXCLUDECNT}" >> ${OUTPUTCSV}

    done

done

