# README #

Shows the source LOC of all the repositories that make up Airstage Cloud.

### Summary ###

* Source counting uses [cloc](https://github.com/AlDanial/cloc/releases/tag/v1.92).
* Every other Wednesday(JST), the sources are counted by [bitbucket-pipeline](https://bitbucket.org/thorikoshi/source_count/addon/pipelines/home#!/results/page/1)
* There are two LOC results.
    - Standard output of pipeline `python print_pandas.py`
    - You can download it from Artifacts in [pipeline](https://bitbucket.org/thorikoshi/source_count/addon/pipelines/home#!/results/page/1).
* Output field description
    - AllCounts : All files are counted.
    - ExcludeTestCounts : Excludes test files.
* The counting repo and branch are defined in XXX.txt.


---- 

memo  

get all repositories of ascx-ui

```
$ curl --user thorikoshi:<app_pass> "https://api.bitbucket.org/2.0/repositories/fgl-smartbuilding?pagelen=100&q=project.name+%3D+%22ascx-ui%22" | jq -r '.values[].name' | sort
```

